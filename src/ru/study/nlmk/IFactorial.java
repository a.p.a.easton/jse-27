package ru.study.nlmk;

public interface IFactorial {
    int factor(int f);
}
