package ru.study.nlmk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class InvocationHandlerImpl implements InvocationHandler {

    private Factorial factorial;

    private static final int MAX = 10;

    Map<Integer, Integer> factorHash = new LinkedHashMap<>(){
        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
            return size() > MAX;
        }
    };

    public InvocationHandlerImpl(Factorial factorial) {
        this.factorial = factorial;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("factor")){
            if(factorHash.containsKey(args[0])){
                return factorHash.get(args[0]);
            }
            else{
                Integer result = (Integer)method.invoke(factorial, args[0]);
                factorHash.put((Integer)args[0], result);
                return result;
            }
        }
        return method.invoke(factorial, args);
    }
}
