package ru.study.nlmk;

public class Factorial implements IFactorial{

    @Override
    public int factor(int f){
        if(f==1){
            return f;
        }
        return f*factor(f-1);
    }

}
